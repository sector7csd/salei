kernels:
  - name: Linux-Kernel
    packages:
      - linux
      - linux-headers
  - name: Linux-LTS-Kernel
    packages:
      - linux-lts
      - linux-lts-headers
  - name: Linux-Zen-Kernel
    packages:
      - linux-zen
      - linux-zen-headers
  - name: Linux-Hardened-Kernel
    packages:
        - linux-hardened
        - linux-hardened-headers
groups:
  - name: Microcode
    packages:
      - amd-ucode
      - intel-ucode
  - name: efi
    packages:
      - efibootmgr
      - dosfstools
      - mtools
  - name: Basic services
    packages:
      - bash-completion
      - acpid
      - cronie
      - avahi
      - python
      - man
      - usbutils
      - pacman-contrib
      - os-prober
    exec:
      - systemctl enable acpid.service avahi-daemon cronie systemd-timesyncd.service paccache.timer
  - name: Extra filesystems
    packages:
      - exfat-utils
      - ntfs-3g
      - dosfstools
      - sshfs
      - xfsprogs
      - gvfs-smb
  - name: xorg
    packages:
      - xorg-server
      - xorg-xinit
      - xorg-xwininfo
      - wmctrl
      - xorg-drivers
      - virtualbox-guest-utils
    exec:
      - systemctl enable vboxservice.service
  # Light Display Manager
  - name: lightdm
    packages:
      - lightdm
      - lightdm-gtk-greeter
      - lightdm-gtk-greeter-settings
    exec:
      - systemctl enable lightdm.service
  # Gnome Display Manager
  - name: gdm
    packages:
      - gdm
    exec:
      - systemctl enable gdm.service
  # KDE Display Manager
  - name: sddm
    packages:
      - sddm
      - sddm-kcm
  # LXDE Display Manager
  - name: lxdm
    packages:
      - lxdm
    exec:
      - systemctl enable lxdm.service
  - name: networkmanager
    packages:
      - networkmanager
      - networkmanager-openvpn
    exec:
      - systemctl enable NetworkManager
  - name: networkmanager gui
    packages:
      - network-manager-applet
      - nm-connection-editor
  - name: Pulseaudio
    packages:
      - pulseaudio
      - pavucontrol
      - alsa-utils
  - name: Light Locker
    packages:
      - light-locker
  - name: Awesome
    packages:
      - awesome
      - xterm
      - dmenu
      - compton
      - nitrogen
      - pcmanfm
      - terminus-font-otb
      - lxappearance
      - rofi
  - name: Cinnamon
    packages:
      - cinnamon
      - cinnamon-translations
      - nemo
      - nemo-fileroller
  - name: XFCE
    packages:
      - xfce4
      - xfce4-goodies
      - xfburn
      - ristretto
    exec:
      - xfconf-query -c xfce4-session -p /general/LockCommand -s "xfce4-screensaver-command --lock" --create -t string
  - name: LXDE
    packages:
      - lxde
  - name: LXQT
    packages:
      - lxqt
      - oxygen-icons
  - name: KDE
    packages:
      - plasma-meta
      - kde-applications-meta
      - weston
      - plasma-wayland-session
  - name: Mate
    packages:
      - mate
      - mate-extra
      - mate-applet-dock
      - mate-applet-streamer
  - name: Gnome
    packages:
      - gnome
      - gnome-extra
  - name: Budgie
    packages:
      - budgie-desktop
      - gnome-control-center
      - nautilus
      - gnome
  - name: Deepin
    packages:
      - deepin
      - deepin-extra
  - name: IceWM
    packages:
      - icewm
  - name: fonts
    packages:
      - ttf-fira-code
      - ttf-fira-sans
      - noto-fonts
  - name: icons
    packages:
      - papirus-icon-theme
      - adapta-gtk-theme
      - arc-gtk-theme
      - arc-icon-theme
  - name: bluetooth
    packages:
      - pulseaudio-bluetooth
      - blueman
    exec:
      - systemctl enable bluetooth.service
  - name: tools cli
    packages:
      - tmux
      - mc
      - btop
      - git
      - wget
      - curl
      - zip
      - unzip
      - cmus
      - neofetch
      - net-tools
      - hdparm
      - zsh
      - rsync
      - dos2unix
      - nano
      - lsd
      - mesa-demos
      - mpv
      - newsboat
      - youtube-dl
      - traceroute
      - fd
      - ripgrep
      - ncdu
  - name: tools gui
    packages:
      - terminator
      - vivaldi
      - vlc
      - hunspell-de
      - thunderbird
      - thunderbird-i18n-de
      - audacity
      - atom
      - mousepad
      - pkgfile
      - libcurl-gnutls
      - owncloud-client
      - guvcview
      - discord
  - name: tools gnome
    packages:
      - gnome-calculator
      - gnome-system-monitor
      - gnome-screenshot
      - gnome-disk-utility
      - file-roller
      - gimp
      - evince
      - eog
      - transmission-gtk
      - brasero
      - gnome-font-viewer
      - libgnome-keyring
      - seahorse
  - name: tools dev
    packages:
      - cmake
      - log4cpp
      - tinyxml2
      - libpng
      - sdl2
      - sdl2_image
      - sdl2_gfx
      - openal
      - libsndfile
      - libmodplug
      - tiled
      - gdb
      - graphviz
      - doxygen
      - meld
      - diff-so-fancy
      - bless
  - name: office
    packages:
      - libreoffice-fresh
      - libreoffice-fresh-de
  - name: cockpit
    packages:
      - cockpit
      - cockpit-machines
      - cockpit-pcp
      - cockpit-podman
      - virt-install
      - bridge-utils
      - ebtables
      - dnsmasq
      - openbsd-netcat
      - firewalld
      - packagekit
    exec:
      - systemctl enable cockpit.socket
      - systemctl enable firewalld.service
      - firewall-offline-cmd --service=ssh --service=cockpit
  - name: wallpapers
    packages:
      - archlinux-wallpaper
profiles:
  - name: Minimal
    groups:
      - Basic services
      - Extra filesystems
      - Microcode
      - networkmanager
  - name: Server with Cockpit
    groups:
      - cockpit
      - tools cli
    requires:
      - Minimal
  - name: Gui
    hidden: true
    groups:
      - xorg
      - networkmanager gui
      - bluetooth
      - fonts
      - icons
      - tools cli
      - tools gui
      - tools dev
      - office
      - wallpapers
  # Light Display Manager
  - name: lightdm
    hidden: true
    groups:
      - lightdm
  # GNOME Display Manager
  - name: gdm
    hidden: true
    groups:
      - gdm
  # KDE Display Manager
  - name: sddm
    hidden: true
    groups:
      - sddm
  # LXDE Display Manager
  - name: lxdm
    hidden: true
    groups:
      - lxdm
  - name: tools gnome
    hidden: true
    groups:
      - tools gnome
  # Window Manager: Awesome
  - name: Awesome
    groups:
      - Awesome
    requires:
      - Minimal
      - Gui
      - lightdm
  # Desktop environment: Budgie
  - name: Budgie
    groups:
      - Budgie
    requires:
      - Minimal
      - Gui
      - gdm
      - tools gnome
  # Desktop environment: Cinnamon
  - name: Cinnamon
    groups:
      - Cinnamon
    requires:
      - Minimal
      - Gui
      - lightdm
      - tools gnome
  # Desktop environment: Deepin
  - name: Deepin
    groups:
      - Deepin
    requires:
      - Minimal
      - Gui
      - lightdm
      - tools gnome
  # Desktop environment: Gnome
  - name: Gnome
    groups:
      - Gnome
    requires:
      - Minimal
      - Gui
      - gdm
  # Window manager: IceWM
  - name: IceWM
    groups:
      - IceWM
    requires:
      - Minimal
      - Gui
      - lightdm
  # Desktop environment: KDE
  - name: KDE
    groups:
      - KDE
    requires:
      - Minimal
      - Gui
      - sddm
  # Desktop environment: LXDE
  - name: LXDE
    groups:
      - LXDE
      - Light Locker
    requires:
      - Minimal
      - Gui
      - lxdm
  # Desktop environment: LXQT
  - name: LXQT
    groups:
      - LXQT
    requires:
      - Minimal
      - Gui
      - sddm
  # Desktop environment: Mate
  - name: Mate
    groups:
      - Mate
    requires:
      - Minimal
      - Gui
      - lightdm
  # Desktop environment: XFCE
  - name: XFCE
    groups:
      - Light Locker
      - XFCE
      - Pulseaudio
    requires:
      - Minimal
      - Gui
      - lightdm
