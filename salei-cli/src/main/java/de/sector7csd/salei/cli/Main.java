package de.sector7csd.salei.cli;

import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import de.sector7csd.salei.core.SaleiApp;
import de.sector7csd.salei.core.controller.InstallationTask;
import de.sector7csd.salei.core.data.Profile;
import de.sector7csd.salei.core.data.User;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.sector7csd.salei.cli.TuiUtils.*;

@Slf4j
public class Main extends SaleiApp
{
    public static final int         PREF_WIDTH      = 55;

    protected Screen                screen          = null;
    protected WindowBasedTextGUI    textGUI         = null;
    protected Window                window          = null;
    protected Panel                 contentPanel    = null;

    public static void main(String[] args)
    {
        new Main();
    }

    public Main()
    {
        if(!init())
        {
            System.exit(0);
        }

        val termFactory = new DefaultTerminalFactory();
        try
        {
            screen = termFactory.createScreen();
            screen.startScreen();

            textGUI = new MultiWindowTextGUI(screen);
            window  = new BasicWindow("Salei - Sector 7's Arch Linux Easy Installer");

            contentPanel = new Panel(new GridLayout(2));
            GridLayout gridLayout = (GridLayout)contentPanel.getLayoutManager();

            // --- Select kernel ---
            val kernelBox = addLabeledComboBox(contentPanel,"Selected linux kernel:", model.getPackages().getKernels(), PREF_WIDTH);

            // --- Select profile ---
            val profileList = new ArrayList<Profile>();
            for(val profile : model.getPackages().getProfiles())
            {
                if(!profile.isHidden())
                {
                    profileList.add(profile);
                }
            }

            val profileBox = addLabeledComboBox(contentPanel, "Selected install profile:", profileList, PREF_WIDTH);

            addHorizontalLine(contentPanel, 2);

            // BIOS bootloader
            TextBox tmp = null;
            if(!model.isUefiMode())
            {
                tmp = addLabeledTextBox(contentPanel, "Bootloader location:", model.getBootloaderLocation(), PREF_WIDTH);
                addHorizontalLine(contentPanel, 2);
            }

            TextBox biosBootLoaderLocation = tmp;

            // --- Hostname ---
            val hostName = addLabeledTextBox(contentPanel, "Hostname:", model.getHostname(), PREF_WIDTH);

            addHorizontalLine(contentPanel, 2);

            // --- Local mirror ---
            val useLocalMirror= addLabeledCheckBox(contentPanel, "Use local mirror:", false);
            val localMirror = addLabeledTextBox(contentPanel, "Local mirror:", model.getLocalMirror(), PREF_WIDTH);

            addHorizontalLine(contentPanel, 2);
            
            // --- Install yay ---
            val installYay = addLabeledCheckBox(contentPanel, "Install yay:", model.isInstallYay());

            addHorizontalLine(contentPanel, 2);

            // --- Locales ---

            val timeZone = addLabeledComboBox(contentPanel, "Time zone:", model.getTimeZones(), PREF_WIDTH);
            timeZone.setDropDownNumberOfRows(20);
            timeZone.setSelectedItem(model.getSelectedTimeZone());

            val keyboardLayout = addLabeledComboBox(contentPanel, "Keyboard layout:", model.getKeyboardLayouts(), PREF_WIDTH);
            keyboardLayout.setSelectedItem(model.getSelectedKeyboardLayout());

            val locale = addLabeledComboBox(contentPanel, "Locale:", model.getSystemLocales(), PREF_WIDTH);
            locale.setDropDownNumberOfRows(20);
            locale.setSelectedItem(model.getSelectedSystemLocale());

            addHorizontalLine(contentPanel, 2);

            // --- Root password ---
            val rootPassword = addLabeledPasswordBox(contentPanel, "Root-password:", model.getRootPassword(), PREF_WIDTH);
            val rootPasswordRepeat = addLabeledPasswordBox(contentPanel, "Root-password repeat:", model.getRootPassword(), PREF_WIDTH);

            addHorizontalLine(contentPanel, 2);

            // --- User ---
            val userName = addLabeledTextBox(contentPanel, "User name:", "", PREF_WIDTH);
            val userPassword = addLabeledPasswordBox(contentPanel, "User password:", "", PREF_WIDTH);
            val userPasswordRepeat = addLabeledPasswordBox(contentPanel, "User password repeat:", "", PREF_WIDTH);
            val fullName = addLabeledTextBox(contentPanel, "Real name:", "", PREF_WIDTH);
            val userShell = addLabeledTextBox(contentPanel, "User Shell:", "/bin/bash", PREF_WIDTH);
            val userSudo= addLabeledCheckBox(contentPanel, "Sudo permission:", false);

            addHorizontalLine(contentPanel, 2);

            val button = new Button("Start", new Runnable()
            {
                @Override
                public void run()
                {
                    // User input validation
                    String msg = "";
                    if(!rootPassword.getText().equals(rootPasswordRepeat.getText()))
                    {
                        msg += "root password mismatches\n";
                    }

                    if(rootPassword.getText().length() == 0)
                    {
                        msg += "root password is empty\n";
                    }

                    if(!userPassword.getText().equals(userPasswordRepeat.getText()))
                    {
                        msg += "user password mismatches\n";
                    }

                    if(userPassword.getText().length() == 0)
                    {
                        msg += "user password is empty\n";
                    }

                    if(userName.getText().length() == 0)
                    {
                        msg += "user name is empty\n";
                    }

                    val userNameLowerCase = userName.getText().toLowerCase();
                    if(!userName.getText().equals(userNameLowerCase))
                    {
                        msg += "user name contains upper case letters\n";
                    }

                    Pattern pattern = Pattern.compile("^(\\d+.*|-\\d+.*)");
                    Matcher matcher = pattern.matcher(userName.getText());

                    if(matcher.matches())
                    {
                        msg += "user name starts with a number\n";
                    }

                    // start install only when there is no error message
                    if(msg.length() == 0)
                    {
                        window.close();

                        try
                        {
                            screen.close();
                        }
                        catch (IOException e)
                        {
                            log.error("Exception: Can't close screen", e);
                        }

                        log.info("Yay! Start install!");

                        // Update our install model according to the ui
                        model.setSelectedProfile(profileBox.getSelectedItem().getName());
                        model.setSelectedKernel(kernelBox.getSelectedItem().getName());

                        if(biosBootLoaderLocation != null)
                        {
                            model.setBootloaderLocation(biosBootLoaderLocation.getText());
                        }

                        model.setRootPassword(rootPassword.getText());
                        model.setRootPasswordRepeat(rootPasswordRepeat.getText());

                        model.setHostname(hostName.getText());

                        model.setUseLocalMirror(useLocalMirror.isChecked());
                        model.setLocalMirror(localMirror.getText());

                        model.setSelectedTimeZone(timeZone.getSelectedItem());
                        model.setSelectedKeyboardLayout(keyboardLayout.getSelectedItem());
                        model.setSelectedSystemLocale(locale.getSelectedItem());

                        model.setInstallYay(installYay.isChecked());

                        if(!userName.getText().isEmpty() && !userPassword.getText().isEmpty())
                        {
                            model.getUsers().add(User.builder() .name(userName.getText())
                                    .password(userPassword.getText())
                                    .passwordRepeat(userPasswordRepeat.getText())
                                    .fullName(fullName.getText())
                                    .shell(userShell.getText())
                                    .sudoRight(userSudo.isChecked())
                                    .build());
                        }

                        // start install
                        val task = new InstallationTask(model, config);
                        task.run();

                        while(model.getInstallState() != InstallationModel.InstallState.DONE)
                        {
                            try
                            {
                                Thread.sleep(250);
                            }
                            catch(InterruptedException e)
                            {
                            }
                        }

                        System.exit(0);
                    }
                    else
                    {
                        // Display errors otherwise
                        MessageDialog.showMessageDialog(textGUI, "Error", msg);
                    }
                }
            })
            .setLayoutData(GridLayout.createHorizontallyEndAlignedLayoutData(2));
            contentPanel.addComponent(button);

            window.setComponent(contentPanel);

            textGUI.addWindowAndWait(window);

        }
        catch(Exception e)
        {
            log.error("Exception", e);
        }

        System.exit(0);
    }
}
