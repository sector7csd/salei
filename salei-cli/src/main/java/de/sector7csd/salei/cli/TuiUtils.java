package de.sector7csd.salei.cli;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import lombok.val;

import java.util.List;

public class TuiUtils
{
    public static <T> ComboBox<T> addLabeledComboBox(Panel contentPanel, String label, List<T> data, int prefWidth)
    {
        contentPanel.addComponent(new Label(label));

        val comboBox = new ComboBox<T>(data);
        comboBox.setReadOnly(true);
        comboBox.setPreferredSize(new TerminalSize(prefWidth, 1));
        contentPanel.addComponent(comboBox);

        return comboBox;
    }

    public static TextBox addLabeledPasswordBox(Panel contentPanel, String label, String filled, int prefWidth)
    {
        return addLabeledTextBox(contentPanel, label, filled, prefWidth, "*");
    }

    public static TextBox addLabeledTextBox(Panel contentPanel, String label, String filled, int prefWidth)
    {
        return addLabeledTextBox(contentPanel, label, filled, prefWidth, null);
    }

    public static TextBox addLabeledTextBox(Panel contentPanel, String label, String filled, int prefWidth, String passwordMask)
    {
        val textBox = new TextBox()
                                .setPreferredSize(new TerminalSize(prefWidth, 1));


        if(passwordMask != null)
        {
            textBox.setMask('*');
        }

        if(filled != null)
        {
            textBox.setText(filled);
        }

        contentPanel.addComponent(new Label(label));
        contentPanel.addComponent(textBox);

        return textBox;
    }

    public static CheckBox addLabeledCheckBox(Panel contentPanel, String label, boolean checked)
    {
        contentPanel.addComponent(new Label(label));
        val checkBox = new CheckBox("");
        checkBox.setChecked(checked);
        contentPanel.addComponent(checkBox);

        return checkBox;
    }

    public static void addHorizontalLine(Panel contentPanel, int span)
    {
        contentPanel.addComponent(
                new Separator(Direction.HORIZONTAL)
                        .setLayoutData(
                                GridLayout.createHorizontallyFilledLayoutData(2)));
    }
}
