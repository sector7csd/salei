#!/usr/bin/env bash

. colors.sh

function show_help() {
  echo "Usage:"
  writeParam "-h" "help    " "shows this help"
  writeParam "-d" "device: " "The device which should be used (e.g. /dev/sda2 or /dev/mapper/cryptsys)"
  writeParam "-n" "name:   " "Name for /dev/mapper"
  exit 0
}

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables
device=""
name=""

# Parsing commandline options

while getopts "h?d:n:" opt; do
    case "$opt" in
    h|\?)
        show_help
        ;;
    d)  device=$OPTARG
        ;;
    n)  name=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

# Checking for mandatory parameters

if [ -z "$device"  ] || [ -z "$name" ]; then
  writeError "Missing arguments"
  show_help
fi

# Parameter validation

if [ ! -b "$device" ]; then
  writeError "Device [${green}$device${red}] is not existing${normal}"
  exit 1
fi

# Print selected options

writeInfo "Selected options:"
echo "- device:         [${paramInfo}$device${normal}]"
echo "- name:           [${paramInfo}$name${normal}]"

# Execution

writeInfo "-- encrypting [${paramInfo}$device${normal}] --"
cryptsetup luksFormat --type luks1 "$device" || abortOnError 1 "cryptsetup luksFormat failed"
cryptsetup open "$device" "$name" || abortOnError 2 "cryptsetup open failed"

UUID=$(blkid -s UUID -o value "$device")

writeInfo "Note:"
echo "When this partition is used as root partition - be sure to add the following lines to ${bold}${blue}/etc/default/grub${normal}"
writeInfo "GRUB_ENABLE_CRYPTODISK=y"
writeInfo "GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=$UUID:$name\""