#!/usr/bin/env bash

. colors.sh

function show_help() {
  echo "Usage:"
  writeParam "-h" "help          " "shows this help"
  writeParam "-d" "device:       " "The device which should be used (e.g. /dev/sda2 or /dev/mapper/cryptsys)"
  writeParam "-m" "mountPoint:   " "Where the device should be mounted"
  writeParam "-s" "swapSize:     " "Size of the swap in MiB"
  writeParam "-c" "compression:  " "Enables compression"
  writeParam "-r" "rootPartition:" "When set, the sub volumes for ${paramInfo}@${normal}, ${paramInfo}@home${normal}, ${paramInfo}@log${normal}, ${paramInfo}@swap${normal}, ${paramInfo}@pkg${normal} and ${paramInfo}@snapshots${normal} will be created"
  echo "                  When not set, the sub volumes ${paramInfo}@data${normal} will be created"
  exit 0
}

function generateMountOptions() {

  # $1 compression
  compressionOption=""

  if [ "$1" -eq 1 ]; then
    compressionOption="compress"
  fi

  generateMountOptions="noatime,$compressionOption,space_cache=v2"
}

function mountBtrfs() {

  # $1 sub volume
  # $2 device
  # $3 mount point
  # $4 compression

  generateMountOptions "$4"

  writeInfo "command: ${magenta}mount -o $generateMountOptions,subvol=\"$1\" \"$2\" \"$3\"${normal}"
  mount -o $generateMountOptions,subvol="$1" "$2" "$3"
}

function remount() {

  # $1 device
  # $2 mount point
  # $3 default sub volume id
  # $4 compression

  writeInfo "-- set default and remount"
  btrfs sub set-default "$3" "$2"
  umount "$2"

  generateMountOptions "$4" "$5"
  writeInfo "command: ${magenta}mount -o $generateMountOptions \"$1\" \"$2\"${normal}"
  mount -o $generateMountOptions "$1" "$2"
}

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables
device=""
mountPoint=""
compression=0
rootPartition=0
declare -i swapSize=0

# Parsing commandline options

while getopts "h?:d:m:s:rc" opt; do
    case "$opt" in
    h|\?)
        show_help
        ;;
    d)  device=$OPTARG
        ;;
    m)  mountPoint=$OPTARG
        ;;
    s)  swapSize=$OPTARG
        ;;
    c)  compression=1
        ;;
    r)  rootPartition=1
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

# Checking for mandatory parameters

if [ -z "$device"  ] || [ -z "$mountPoint" ]; then
  writeError "Missing arguments"
  show_help
fi

# Parameter validation

if [ ! -b "$device" ]; then
  writeError "Device [${green}$device${red}] is not existing"
  exit 1
fi

if [ ! -d "$mountPoint" ]; then
  writeError "Mount point [${green}$mountPoint${red}] is not existing"
fi

# Print selected options

writeInfo "Selected options:"
echo "- device:         [${green}$device${normal}]"
echo "- mountPoint:     [${green}$mountPoint${normal}]"
echo "- swapSize:       [${green}$swapSize${normal}] MiB"
echo "- compression:    [${green}$compression${normal}]"
echo "- rootPartition:  [${green}$rootPartition${normal}]"

# Execution

if [ $rootPartition -eq 1 ]; then

  writeInfo "-- create btrfs"
  mkfs.btrfs -f -L "ROOT" "$device" || abortOnError 1 "mkfs.btrfs failed"

  writeInfo "-- mounting"
  mount "$device" "$mountPoint" || abortOnError 2 "mounting failed"

  writeInfo "-- create sub volumes for a root partition"
  btrfs sub cr "$mountPoint/@" || abortOnError 3 "sub volume creation failed"
  btrfs sub cr "$mountPoint/@home" || abortOnError 3 "sub volume creation failed"
  btrfs sub cr "$mountPoint/@log" || abortOnError 3 "sub volume creation failed"

  if [ "$swapSize" -ne 0 ]; then
    btrfs sub cr "$mountPoint/@swap" || abortOnError 3 "sub volume creation failed"
  fi

  btrfs sub cr "$mountPoint/@pkg" || abortOnError 3 "sub volume creation failed"
  btrfs sub cr "$mountPoint/@snapshots" || abortOnError 3 "sub volume creation failed"

  remount "$device" "$mountPoint" 256 $compression

  writeInfo "-- create mount points"
  mkdir -p "$mountPoint/boot/EFI"
  mkdir -p "$mountPoint/home"
  mkdir -p "$mountPoint/var/log"

  if [ "$swapSize" -ne 0 ]; then
    mkdir -p "$mountPoint/swap"
  fi

  mkdir -p "$mountPoint/var/cache/pacman/pkg"
  mkdir -p "$mountPoint/.snapshots"

  writeInfo "-- mounting sub volumes"
  mountBtrfs "@home" "$device" "$mountPoint/home" $compression
  mountBtrfs "@log" "$device" "$mountPoint/var/log" $compression

  if [ "$swapSize" -ne 0 ]; then
    mountBtrfs "@swap" "$device" "$mountPoint/swap" $compression
  fi

  mountBtrfs "@pkg" "$device" "$mountPoint/var/cache/pacman/pkg" $compression
  mountBtrfs "@snapshots" "$device" "$mountPoint/.snapshots" $compression

  if [ "$swapSize" -ne 0 ]; then

    swapFile=$mountPoint/swap/swapFile

    writeInfo "-- create swap file"
    truncate -s 0 "$swapFile"
    chattr +C "$swapFile"
    btrfs property set "$swapFile" compression none

    dd if=/dev/zero of="$swapFile" bs=1M count="$swapSize" status=progress

    chmod 600 "$swapFile"
    mkswap "$swapFile"
    swapon "$swapFile"

  fi

else

  writeInfo "-- create btrfs"
  mkfs.btrfs -f -L "DATA" "$device" || abortOnError 1 "mkfs.btrfs failed"

  writeInfo "-- mounting"
  mount "$device" "$mountPoint" || abortOnError 2 "mounting failed"

  writeInfo "-- create sub volumes for a data partition"
  btrfs sub cr "$mountPoint/@data" || abortOnError 3 "sub volume creation failed"
  btrfs sub cr "$mountPoint/@snapshots" || abortOnError 3 "sub volume creation failed"

  remount "$device" "$mountPoint" 256 $compression

  writeInfo "-- create mount points"
  mkdir -p "$mountPoint/.snapshots"

  writeInfo "-- mounting sub volumes"
  mountBtrfs "@snapshots" "$device" "$mountPoint/.snapshots" $compression

fi

writeInfo "done"
