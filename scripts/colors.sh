#!/usr/bin/env bash

bold=$(tput bold)
normal=$(tput sgr0)
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
white=$(tput setaf 7)

err=$red
warn=$yellow
info=$blue
param=$white
paramInfo=$green

function writeError() {
  echo "${bold}${err}$1${normal}"
}

function writeWarning() {
  echo "${bold}${warn}$1${normal}"
}

function writeInfo() {
 echo "${bold}${info}$1${normal}"
}

function writeParam() {
  # $1 parameter
  # $2 parameter name
  # $3 description

 echo "${bold}${param}$1 ${info}$2${normal} $3${normal}"
}

function abortOnError() {
    writeError "$2"
    exit "$1"
}