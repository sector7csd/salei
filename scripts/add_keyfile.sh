#!/usr/bin/env bash

. colors.sh

function show_help() {
  echo "Usage:"
  writeParam "-h" "help        " "shows this help"
  writeParam "-d" "device:     " "The device which should be used (e.g. /dev/sda2 or /dev/mapper/cryptsys)"
  writeParam "-m" "mountPoint: " "Where the device is mounted"
  exit 0
}

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables
device=""
name=""

# Parsing commandline options

while getopts "h?d:m:" opt; do
    case "$opt" in
    h|\?)
        show_help
        ;;
    d)  device=$OPTARG
        ;;
    m)  mountPoint=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

# Checking for mandatory parameters

if [ -z "$device"  ] || [ -z "$mountPoint" ]; then
  writeError "Missing arguments"
  show_help
fi

# Parameter validation

if [ ! -b "$device" ]; then
  writeError "Device [${green}$device${red}] is not existing"
  exit 1
fi

if [ ! -d "$mountPoint" ]; then
  writeError "Mount point [${green}$mountPoint${red}] is not existing"
fi

# Print selected options

writeInfo "Selected options:"
echo "- device:         [${green}$device${normal}]"
echo "- mountPoint:     [${green}$mountPoint${normal}]"

# Execution
#chmod 600 /mnt/boot/initramfs-linux*
dd bs=512 count=4 if=/dev/urandom of="$mountPoint/crypto_keyfile.bin"
chmod 400 "$mountPoint/crypto_keyfile.bin"
cryptsetup luksAddKey "$device" "$mountPoint/crypto_keyfile.bin"

writeInfo "Note:"
echo "Please add to ${bold}${blue}/etc/mkinitcpio.conf${normal} the line ${bold}${blue}FILES(/crypto_keyfile.bin)${normal}"
echo "then call ${bold}${blue}mkinitcpio -p linux${normal}"
