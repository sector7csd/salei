package de.sector7csd.salei.web.resources;

import de.sector7csd.salei.core.controller.InstallationTask;
import de.sector7csd.salei.core.data.BaseItem;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import de.sector7csd.salei.core.utils.InstallerUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Slf4j
@Path("/api")
public class InstallerResource
{
    protected InstallationModel model;
    protected Config            config;

    public InstallerResource(InstallationModel model, Config config)
    {
        this.model  = model;
        this.config = config;
    }

    @GET
    @Path("/getModel")
    @Produces(MediaType.APPLICATION_JSON)
    public InstallationModel getModel()
    {
        return model;
    }

    @POST
    @Path("/startInstall")
    @Produces(MediaType.APPLICATION_JSON)
    public InstallationModel startInstall(InstallationModel inModel)
    {
        log.info("inModel: [{}] ", inModel);

        if(model.getInstallState() == InstallationModel.InstallState.READY_TO_INSTALL)
        {
            if( InstallerUtils.isStringInBaseItemList(inModel.getSelectedProfile(),    model.getPackages().getProfiles())   &&
                InstallerUtils.isStringInBaseItemList(inModel.getSelectedKernel(),     model.getPackages().getKernels())    &&
                model.getTimeZones().contains(inModel.getSelectedTimeZone())                                                &&
                model.getSystemLocales().contains(inModel.getSelectedSystemLocale())                                        &&
                model.getKeyboardLayouts().contains(inModel.getSelectedKeyboardLayout()))
            {
                model.setSelectedKernel         (inModel.getSelectedKernel());
                model.setSelectedProfile        (inModel.getSelectedProfile());
                model.setSelectedKeyboardLayout (inModel.getSelectedKeyboardLayout());
                model.setSelectedSystemLocale   (inModel.getSelectedSystemLocale());
                model.setSelectedTimeZone       (inModel.getSelectedTimeZone());

                model.setRootPassword           (inModel.getRootPassword());
                model.setRootPasswordRepeat     (inModel.getRootPasswordRepeat());
                model.setUsers                  (inModel.getUsers());

                model.setUseLocalMirror         (inModel.isUseLocalMirror());
                model.setLocalMirror            (inModel.getLocalMirror());

                model.setHostname               (inModel.getHostname());

                model.setInstallState(InstallationModel.InstallState.INSTALLING);

                val thread = new Thread(new InstallationTask(model, config));
                thread.start();
            }
        }

        return model;
    }
}
