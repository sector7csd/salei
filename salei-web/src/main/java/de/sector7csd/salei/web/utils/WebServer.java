package de.sector7csd.salei.web.utils;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import java.util.List;

import static org.eclipse.jetty.servlet.ServletContextHandler.NO_SESSIONS;

@Slf4j
public class WebServer
{
    protected HandlerList handlerList = new HandlerList();

    public void start(int port) throws Exception
    {
        start(port, null);
    }

    public void start(int port, List<Object> resources) throws Exception
    {
        start(port, "./www-root", "index.html", resources);
    }

    public void start(int port, String wwwRoot, String welcomeFile, List<Object> resources) throws Exception
    {
        // Create server
        val server                  = new Server(port);

        // Create resources
        val contentHandler = addWebFolder("/", wwwRoot, true);
        contentHandler.setWelcomeFiles(new String[]{ welcomeFile });

        // Add Servlets
        if(resources != null)
        {
            registerResources(resources);
        }

        // Start
        server.setHandler(handlerList);

        server.start();
        server.dumpStdErr();
        server.join();

        log.info("server started");
    }

    protected ResourceHandler addWebFolder(String webDir, String fileSystemDir, boolean directoryListing)
    {
        val contentHandler = new ResourceHandler();
        contentHandler.setResourceBase(fileSystemDir);
        contentHandler.setDirectoriesListed(directoryListing);

        val resCtxHandler = new ContextHandler(webDir);
        resCtxHandler.setHandler(contentHandler);

        log.info("adding locale directory: [" + fileSystemDir + "] as web directory: [" + webDir + "]");

        handlerList.addHandler(resCtxHandler);

        return contentHandler;
    }

    protected void registerResources(List<Object> resourceList)
    {
        val ctx = new ServletContextHandler(NO_SESSIONS);
        ctx.setContextPath("/");

        val rc                     = new ResourceConfig();

        for(val resource : resourceList)
        {
            log.info("registering resource [{}]", resource);
            rc.register(resource);
        }

        val servletContainer       = new ServletContainer(rc);
        val servletHolder          = new ServletHolder(servletContainer);
        ctx.addServlet(servletHolder, "/*");

        handlerList.addHandler(ctx);
    }
}
