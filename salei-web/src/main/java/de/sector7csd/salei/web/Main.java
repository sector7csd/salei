package de.sector7csd.salei.web;

import de.sector7csd.salei.core.SaleiApp;
import de.sector7csd.salei.web.resources.InstallerResource;
import de.sector7csd.salei.web.utils.WebServer;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.Arrays;

@Slf4j
public class Main extends SaleiApp
{
    public static void main(String[] args)
    {
        log.info("Hello World");

        new Main();
    }

    public Main()
    {
        if(!init())
        {
            System.exit(0);
        }

        try
        {
            val webServer = new WebServer();
            Object[] resources = {new InstallerResource(model, config)};
            webServer.start(8080, Arrays.asList(resources));
        }
        catch(Exception e)
        {
            log.error("Exception", e);
        }

    }
}
