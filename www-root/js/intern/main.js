const app = angular.module('saleiApp', ['ngAnimate', 'ui.bootstrap']);

app.controller('saleiCtrl', ['$scope', '$location', '$anchorScroll', '$http', '$interval', '$timeout',
    function($scope, $location, $anchorScroll, $http, $interval, $timeout)
    {
        const logger = Logger.create('main');

        logger.info("start up");

        const ajax = new SaleiAjax($http, $scope);

        $scope.setupOk = true;

        $scope.startInstall = function()
        {
            logger.info(`Checking inputs...`);

            let ok = true;

            ok &= $scope.installModel.rootPassword == $scope.installModel.rootPasswordRepeat;
            if(!ok)
            {
                alert("Root password mismatches");
                return;
            }

            ok &= $scope.installModel.rootPassword !== null;
            if(!ok)
            {
                alert("Root password can not be empty");
                return;
            }

            let msg = "";

            function userValidation(user)
            {
                if(user.password !== user.passwordRepeat)
                {
                    msg += `password for user [${user.name}] mismatches\n`;
                    return false;
                }

                if(user.password === "")
                {
                    msg += `password for user [${user.name}] is empty\n`;
                    return false;
                }

                if(!Number.isNaN(parseInt(user.name)))
                {
                    msg += `user name for user [${user.name}] starts with a number\n`;
                    return false;
                }

                if((/[A-Z]/).test(user.name))
                {
                    msg += `user name for user [${user.name}] contains upper case letters\n`;
                    return false;
                }

                if(user.name === "")
                {
                    msg += `user name is empty\n`;
                    return false;
                }

                return true;
            }

            ok &= ($scope.installModel.users.some(userValidation) || $scope.installModel.users.length == 0);
            if(!ok)
            {
                alert(msg);
                return;
            }

            if (ok)
            {
                logger.info(`Start install, using kernel [${$scope.installModel.selectedKernel}] and profile [${$scope.installModel.selectedProfile}]`);
                ajax.startInstall($scope.installModel);
            }
        }

        $scope.addUser = function ()
        {
            const user =
            {
                shell           : "/bin/bash",
                name            : "",
                password        : "",
                passwordRepeat  : "",
                sudoRight       : false
            }

            $scope.installModel.users.push(user);
        }

        $scope.removeUser = function(user)
        {
            const index = $scope.installModel.users.indexOf(user);
            $scope.installModel.users.splice(index, 1);
            logger.info("deleting user at index: [%d]", index);
        }

        ajax.updateModel(function done()
        {
            logger.info("updateModel done");
        });

        $interval(function() {

            if($scope.installModel !== undefined)
            {
                if($scope.installModel.installState === "INSTALLING")
                {
                    ajax.updateModel(function ()
                    {
                        // Scroll to latest console output item, after the model has been updated
                        const itemCount = $scope.installModel.output.length;
                        const scrollLocation = `console${itemCount-1}`;

                        logger.info(`Scrolling to ${scrollLocation}`);

                        $location.hash(scrollLocation);
                        $anchorScroll();
                    });
                }
            }

        }, 1000);

        logger.info("ready for action");
    }]);
