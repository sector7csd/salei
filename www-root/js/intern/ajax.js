class SaleiAjax
{
    constructor($http, $scope, baseUrl)
    {
        const logger = Logger.create('ajax');

        this.baseUrl    = baseUrl;

        if(this.baseUrl == undefined)
        {
            this.baseUrl = "/api/";
        }

        this.get = function(url, successFunc)
        {
            const fullUrl = this.baseUrl + url;

            logger.info("getting data from url [" + fullUrl + "]");

            $http.get(fullUrl)
                .then(function(data)
                {
                    logger.info("request was successful");

                    if(successFunc != undefined)
                    {
                        successFunc(data.data);
                    }
                });
        }

        this.post = function(url, data, successFunc)
        {
            const fullUrl = this.baseUrl + url;

            logger.info("getting data from url [" + fullUrl + "]");

            const json = angular.toJson(data); //JSON.stringify(data);

            $http.post(fullUrl, json)
                .then(function(data)
                {
                    logger.info("request was successful");

                    if(successFunc != undefined)
                    {
                        successFunc(data.data);
                    }
                });
        }

        this.startInstall = function(installModel)
        {
            this.post("startInstall", installModel, function(data)
            {
                $scope.installModel = data;
                logger.info("install started");
            });
        }

        this.updateModel = function (doneFunc)
        {
            this.get('getModel',function(data)
            {
                $scope.installModel = data;
                logger.info("installModel updated");

                if(doneFunc != undefined)
                {
                    doneFunc();
                }
            });
        }
    }
}