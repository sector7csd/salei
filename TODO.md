# TODO

- ArchBuild-ISO:
  
    - [ ] Clean destination or delete old iso, before copying new iso
    
- kernel stuff:

    - [x] we are using now **nvidia-dkms** which provides us drivers for the normal, lts and the zen kernel
    
    - [ ] Driver detection:
    
        - we need to add an **if** section to the package group, so that we can query for such variables
        
        - the same would be also useful for installing other drivers, e.g. for virtualization
        
        - store the output of **systemd-detect-virt** in a variable
        
    - maybe we should add to option to install multiple kernels

```bash
lspci | grep VGA | grep NVIDIA
01:00.0 VGA compatible controller: NVIDIA Corporation GP104 [GeForce GTX 1070] (rev a1)          
```

- disk setup:

    - [ ] auto mounting / auto partitioning

      - [ ] try **parted** to automatically create partitions 
        The parameter **optimal** ensure the optimum alignment otherwise you'll get performance issues
    
        ```bash
        sudo parted -s -a optimal -- /dev/sdX mkpart primary 1MiB -2048s
        ```

        ```      
        wipefs -a /dev/sda
      
        parted /dev/sda -s -a optimal -- mklabel gpt \
        	unit MiB \
        	mkpart primary fat32 1 351 \
       	 set 1 boot on \
       	 set 1 esp on \
       	 mkpart primary btrfs 351 100% \
       	 quit
        ```
          
   - [ ] allow selecting of bootloader location, currently it's hard coded for BIOS to _/dev/sda_
   
   - [x] allow crypt disk setup
   
   - [ ] BTRFS special setup
   
   - [ ]  
   
   - [ ] Encryption 
   
       - [ ] **/etc/mkinitcpio.conf** - we have to adjust the hooks, if the device isn't encrypted, then
             we have to remove the **encrypt** hook
   
- preconfigure the desktop environments
    - [ ] LDXE
    - [ ] XFCE
         - [ ] Currently, issues with screen locking
            - xfconf-query -c xfce4-session -p /general/LockCommand -s "xtrlock" --create -t string
    - [ ] Cinnamon
    - [ ] KDE
    
- provide specific set of tools for each desktop environment
    - [ ] LDXE
    - [ ] XFCE
        - [x] xfburn
    - [ ] Cinnamon
        - [x] should be mostly complete with gnome tools
    - [ ] KDE   

- fonts

    ```bash
    mkdir ~/.local/share/fonts
    pushd ~/.local/share/fonts
    wget https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode/Regular/complete/Fira%20Code%20Regular%20Nerd%20Font%20Complete.ttf
    popd
    fc-cache -f -v
    ```

    

## Skipped for now

- file/permissions:

    - set default file creation mask: umask 027
    
        | permission type | chmod | permissions |
        |-----------------|-------|-------------|
        | file            | 640   | rw-r-----   |
        | directory       | 750   | rwxr-x---   |

    - directory **execute** permission is required to be able to **cd** into it
    - the default is in **/etc/profile** file
    - arch linux ships per default with an umask of 022 (which is dangerous, in a multi-user environment)
    
    - Skipped due to: 
        - issues when copying from a system which has such umask in place to a new system (rsync -a)          
        - pacman says that /usr has permission of 750 instead of the wanted 755
