#!/usr/bin/env bash

echo "Hello World"

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
curl -s "https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh" | sh
curl -s "https://get.sdkman.io" | bash

mkdir ~/.local/share/fonts
pushd ~/.local/share/fonts
wget https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode/Regular/complete/Fira%20Code%20Regular%20Nerd%20Font%20Complete.ttf
popd
fc-cache -f -v