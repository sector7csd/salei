#!/bin/bash

name="alsa_input.usb-Creative_Technology_Ltd_SB_X-Fi_Surround_5.1_Pro_000000xm-00.analog-stereo"
mic_volume="350%"

id=$(pacmd list-sources | awk '/index:/ {print $0} /name:/ {print $0};' | grep "${name}" -B 1 | grep "index" | cut -d ":" -f 2 | xargs)

if [[ ! -z "${id}" ]]; then
	pactl set-source-volume ${id} ${mic_volume}
else
	echo "Soundcard not corrected"
fi
