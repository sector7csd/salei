#!/bin/bash

SNAPSHOTSDIR=/.snapshots

sudo btrfs subvolume show $SNAPSHOTSDIR 1>/dev/null 2>&1
IS_BTRFS_SUBVOLUME=$?

mount | grep $SNAPSHOTSDIR 1>/dev/null 2>&1
IS_MOUNTED=$?

if [ $IS_BTRFS_SUBVOLUME -eq 0 ] && [ $IS_MOUNTED -eq 0 ]
then

  echo "-- Checking for old snapshots"
  ls "$SNAPSHOTSDIR" -1U | head -n -25 | while IFS= read -r f; do
    echo "--- Removing snapshot [$f]"
    sudo btrfs sub delete $SNAPSHOTSDIR/$f
  done

  SNAPSHOTNAME=ROOT-$(date '+%y-%m-%d_%T')
  echo "-- Creating snapshot [$SNAPSHOTNAME]"
  sudo btrfs subvolume snapshot -r / $SNAPSHOTSDIR/"$SNAPSHOTNAME"

fi