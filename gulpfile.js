const { task, src, dest, series, parallel  } = require('gulp');
const rename = require('gulp-rename');
const del = require('delete');
const argv = require('yargs').argv;

const OUTDIR = "www-root/";
const EXTDIR = "extern/";
const EXTJS  = OUTDIR + "js/"  + EXTDIR;
const EXTCSS = OUTDIR + "css/" + EXTDIR;

const isProduction = (argv.prod === undefined) ? false : true;

function cleanExtJS(cb) {
  del([EXTJS+'/**'], cb);
}

function cleanExtCSS(cb) {
  del([EXTCSS+'/**'], cb);
}

function copyBootstrapJS() {

  const srcName   = isProduction ? "bootstrap.bundle.min.js" : "bootstrap.bundle.js";
  const destName  = "bootstrap.js";

  return src([`node_modules/bootstrap/dist/js/${srcName}`])
          .pipe(rename(destName))
          .pipe(dest(EXTJS));
}

function copyBootstrapCSS() {

  const srcName   = isProduction ? "bootstrap.min.css" : "bootstrap.css";
  const destName  = "bootstrap.css";

  return src([`node_modules/bootstrap/dist/css/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTCSS));
}

function copyUIBootstrapJS() {

  const srcName   = "ui-bootstrap.js";
  const destName  = "ui-bootstrap.js";

  return src([`node_modules/ui-bootstrap4/dist/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTJS));
}

function copyJqueryJS() {

  const srcName   = isProduction ? "jquery.min.js" : "jquery.js";
  const destName  = "jquery.js";

  return src([`node_modules/jquery/dist/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTJS));
}

function copyAngularJS() {

  const srcName   = isProduction ? "angular.min.js" : "angular.js";
  const destName  = "angular.js";

  return src([`node_modules/angular/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTJS));
}

function copyAngularAnimateJS() {

  const srcName   = isProduction ? "angular-animate.min.js" : "angular-animate.js";
  const destName  = "angular-animate.js";

  return src([`node_modules/angular-animate/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTJS));
}

function copyLogPleaseJS() {

  const srcName   = "logplease.min.js";
  const destName  = "logplease.js";

  return src([`node_modules/logplease/dist/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTJS));

}

function copyMomentJS() {

  const srcName   = isProduction ? "/min/moment.min.js" : "/dist/moment.js";
  const destName  = "moment.js";

  return src([`node_modules/moment/${srcName}`])
      .pipe(rename(destName))
      .pipe(dest(EXTJS));
}

exports.clean = parallel(cleanExtJS, cleanExtCSS);

exports.build = series(parallel(cleanExtJS, cleanExtCSS),
                       parallel(copyBootstrapJS, copyBootstrapCSS, copyUIBootstrapJS,
                                copyJqueryJS, copyLogPleaseJS, copyMomentJS,
                                copyAngularJS, copyAngularAnimateJS));

exports.default = exports.build;
