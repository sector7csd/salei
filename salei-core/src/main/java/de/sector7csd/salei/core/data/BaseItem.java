package de.sector7csd.salei.core.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BaseItem
{
    @JsonProperty
    protected String        name;

    @JsonProperty
    protected boolean       hidden  = false;
}
