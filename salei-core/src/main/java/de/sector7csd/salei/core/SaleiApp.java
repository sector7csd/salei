package de.sector7csd.salei.core;

import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.data.Packages;
import de.sector7csd.salei.core.model.InstallationModel;
import de.sector7csd.salei.core.processutils.ProcessExecutor;
import de.sector7csd.salei.core.utils.HDDUtil;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static de.sector7csd.salei.core.utils.InstallerUtils.readFromFile;

@Slf4j
public class SaleiApp
{
    protected static final String               CONFIG_FILE     = "./config/config.yml";
    protected static final String               PKGS_FILE       = "./config/packages.yml";
    protected static final String               UEFI_FIRMWARE   = "/sys/firmware/efi";
    protected static final String               MOUNTPOINT_MNT  = "/mnt";
    protected static final String               MOUNTPOINT_EFI  = "/mnt/boot";

    protected Config config;
    protected Packages packages;

    protected InstallationModel model           = new InstallationModel();

    protected boolean init()
    {
        boolean ok = true;

        config      = (Config)      readFromFile(new File(CONFIG_FILE), Config.class);
        packages    = (Packages)    readFromFile(new File(PKGS_FILE),   Packages.class);

        model.setPackages(packages);

        model.setSelectedTimeZone(config.getDefaultTimeZone());
        model.setSelectedKeyboardLayout(config.getDefaultKeyboardLayout());
        model.setSelectedSystemLocale(config.getDefaultLocale());
        model.setSelectedKernel(config.getDefaultKernel());
        model.setSelectedProfile(config.getDefaultProfile());
        model.setLocalMirror(config.getLocalMirror());

        log.info("Read [{}] kernels, [{}] package groups and [{}] profiles",
                packages.getKernels().size(), packages.getGroups().size(), packages.getProfiles().size());

        val ufi = new File(UEFI_FIRMWARE);
        model.setUefiMode(ufi.exists());
        log.info("System was booted in UEFI mode [{}]", model.isUefiMode());

        ok &= checkMountPoints();

        collectTimeZones();
        collectX11KeyboardLayouts();
        collectLocales();

        return ok;
    }

    protected boolean checkMountPoints()
    {
        val requiredMountPoints = new ArrayList<String>();
        requiredMountPoints.add(MOUNTPOINT_MNT);
        if(model.isUefiMode())
        {
            requiredMountPoints.add(MOUNTPOINT_EFI);
        }

        int goodMountPoints = 0;
        val mounts = HDDUtil.getMapMounts();
        model.setMountpoints(mounts);
        for(val mount : mounts)
        {
            for(String requiredMountPoint : requiredMountPoints)
                if(mount.get(HDDUtil.MOUNTPOINT).equals(requiredMountPoint))
                {
                    log.info("required mount point found: [{}]", mount);
                    goodMountPoints++;
                }
        }

        if(goodMountPoints < requiredMountPoints.size())
        {
            log.error("We're missing the required mount points");
            if(!config.isDebugMode())
            {
                return false;
            }
        }

        return true;
    }

    protected void collectLocales()
    {
        val file = new File(Consts.LOCALE_GEN);

        try
        {
            val reader = new BufferedReader((new FileReader(file)));

            String line;
            while( (line = reader.readLine()) != null)
            {
                if(line.contains(Consts.UTF_8))
                {
                    if(line.startsWith(Consts.COMMENT))
                    {
                        line = line.substring(1);
                    }

                    line = line.trim();

                    if(!model.getSystemLocales().contains(line))
                    {
                        model.getSystemLocales().add(line);
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("Exception", e);
        }
    }

    protected void collectTimeZones()
    {
        collectData(Consts.LIST_TIMEZONES, model.getTimeZones());
    }

    protected void collectX11KeyboardLayouts()
    {
        collectData(Consts.LIST_X11_KEYBOARD_LAYOUTS, model.getKeyboardLayouts());
    }

    protected void collectData(String command, List<String> destList)
    {
        val cmd = config.getCommands().get(command);
        ProcessExecutor.executeProcess(cmd, (line, isStdOut) ->
        {
            if(isStdOut)
            {
                destList.add(line);
            }
        });
    }
}
