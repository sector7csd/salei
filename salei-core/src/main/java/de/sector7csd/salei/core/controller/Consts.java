package de.sector7csd.salei.core.controller;

public class Consts
{
    // --- Files ---
    public static final String                      SHE_BANG_BASH               = "#!/bin/bash";
    public static final String                      BIN_BASH                    = "/bin/bash";
    public static final String                      LOCALE_GEN                  = "/etc/locale.gen";
    public static final String                      ARCH_CHROOT                 = "arch-chroot /mnt";

    // --- Commands ---
    public static final String                      UPDATE_KEYRING              = "UPDATE_KEYRING";
    public static final String                      INSTALL_BASE_SYSTEM         = "INSTALL_BASE_SYSTEM";
    public static final String                      GENERATE_FSTAB              = "GENERATE_FSTAB";
    public static final String                      INSTALL_PKG                 = "INSTALL_PKG";
    public static final String                      EXEC_CHROOT_CMD             = "EXEC_CHROOT_CMD";
    public static final String                      GRUB_INSTALL_BIOS           = "GRUB_INSTALL_BIOS";
    public static final String                      GRUB_INSTALL_UEFI           = "GRUB_INSTALL_UEFI";
    public static final String                      GRUB_MK_CONFIG              = "GRUB_MK_CONFIG";
    public static final String                      GRUB_OS_PROBER              = "GRUB_OS_PROBER";
    public static final String                      COPY_FILES                  = "COPY_FILES";
    public static final String                      SET_PASSWORD                = "SET_PASSWORD";
    public static final String                      CREATE_USER                 = "CREATE_USER";
    public static final String                      DELETE_USER                 = "DELETE_USER";
    public static final String                      CREATE_GROUP                = "CREATE_GROUP";
    public static final String                      DELETE_GROUP                = "DELETE_GROUP";
    public static final String                      ADD_USER_TO_GROUP           = "ADD_USER_TO_GROUP";
    public static final String                      SETUP_SUDO                  = "SETUP_SUDO";
    public static final String                      SETUP_SUDO_YAY              = "SETUP_SUDO_YAY";
    public static final String                      REMOVE_SUDO_YAY             = "REMOVE_SUDO_YAY";
    public static final String                      INSTALL_YAY                 = "INSTALL_YAY";
    public static final String                      SET_HOSTNAME                = "SET_HOSTNAME";
    public static final String                      DELETE_HOMEDIR              = "DELETE_HOMEDIR";
    public static final String                      LIST_TIMEZONES              = "LIST_TIMEZONES";
    public static final String                      SET_TIMEZONE                = "SET_TIMEZONE";
    public static final String                      GENERATE_LOCALES            = "GENERATE_LOCALES";
    public static final String                      LIST_X11_KEYBOARD_LAYOUTS   = "LIST_X11_KEYBOARD_LAYOUTS";
    public static final String                      SET_X11_KEYBOARD_LAYOUT     = "SET_X11_KEYBOARD_LAYOUT";
    public static final String                      ACTIVATE_LOCALE             = "ACTIVATE_LOCALE";
    public static final String                      SET_LOCALE                  = "SET_LOCALE";

    // --- Parameters ---
    public static final String                      USERNAME                    = "***USERNAME***";
    public static final String                      FULLUSERNAME                = "***FULLUSERNAME***";
    public static final String                      PASSWORD                    = "***PASSWORD***";
    public static final String                      SHELL                       = "***SHELL***";
    public static final String                      GROUP                       = "***GROUP***";
    public static final String                      HOSTNAME                    = "***HOSTNAME***";
    public static final String                      PKGLIST                     = "***PKGLIST***";
    public static final String                      BOOTLOADERLOCATION          = "***BOOTLOADERLOCATION***";
    public static final String                      CMD                         = "***CMD***";
    public static final String                      TIMEZONE                    = "***TIMEZONE***";
    public static final String                      KEYBOARD_LAYOUT             = "***KEYBOARD_LAYOUT***";
    public static final String                      LOCALE                      = "***LOCALE***";
    public static final String                      CHROOT                      = "***CHROOT***";

    // --- Other ---
    public static final String                      YAY                         = "yay";
    public static final String                      COMMENT                     = "#";
    public static final String                      UTF_8                       = "UTF-8";
}
