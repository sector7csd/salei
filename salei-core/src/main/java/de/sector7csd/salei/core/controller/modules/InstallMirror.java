package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@Slf4j
public class InstallMirror extends InstallerModule
{
    public static String MIRRORLIST_FILE    = "/etc/pacman.d/mirrorlist";
    public static String SERVER_ENTRY       = "Server = ";

    public InstallMirror(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    @Override
    public List<String> execute()
    {
        if(model.isUseLocalMirror() && !config.isDebugMode())
        {
            try
            {
                // Step one: load file
                List<String> lines = Files.readAllLines(Paths.get(MIRRORLIST_FILE), StandardCharsets.UTF_8);

                // Step two: insert line at the start of the file
                val mirror = String.format("%s%s", SERVER_ENTRY, model.getLocalMirror());
                lines.add(0, mirror);

                // Step three: write updated file
                @Cleanup val writer = new BufferedWriter(new FileWriter(MIRRORLIST_FILE, false));
                for(val line : lines)
                {
                    writer.write(line);
                    writer.newLine();
                }

                writer.flush();
            }
            catch(Exception e)
            {
                log.error("Exception", e);
            }
        }

        return craftedCommands;
    }
}
