package de.sector7csd.salei.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.sector7csd.salei.core.data.BaseItem;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Slf4j
public class InstallerUtils
{
    public static int indexOfList(List<? extends BaseItem> items, String search)
    {
        for(int idx = 0; idx < items.size(); idx++)
        {
            val item = items.get(idx);
            if(item.getName().equals(search))
            {
                return idx;
            }
        }

        return -1;
    }

    public static Object readFromFile(final File file, Class clazz)
    {
        try
        {
            val yamlMapper = new ObjectMapper(new YAMLFactory());
            return yamlMapper.readValue(file, clazz);
        }
        catch (IOException e)
        {
            log.error("Failed phrasing yaml file [{}]", file, e);
        }

        return null;
    }

    public static Object readFromString(final String input, Class clazz)
    {
        try
        {
            val mapper = new ObjectMapper();
            mapper.readValue(input, clazz);
        }
        catch (Exception e)
        {
            log.error("Failed phrasing string [{}]", input, e);
        }

        return null;
    }

    public static boolean isStringInBaseItemList(String searchString, List<? extends BaseItem> baseItemList)
    {
        for(val entry : baseItemList)
        {
            if(entry.getName().equals(searchString))
            {
                return true;
            }
        }

        return false;
    }

    public static int getIndexOfStringInBaseItemList(String searchString, List<? extends BaseItem> baseItemList)
    {
        for(int idx = 0; idx < baseItemList.size(); idx++)
        {
            val entry = baseItemList.get(idx);

            if(entry.getName().equals(searchString))
            {
                return idx;
            }
        }

        return -1;
    }
}
