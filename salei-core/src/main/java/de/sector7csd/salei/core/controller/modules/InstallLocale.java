package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class InstallLocale extends InstallerModule
{
    public InstallLocale(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    public InstallLocale(Map<String, Command> cmds, InstallationModel model, Config config, List<String> craftedCommands)
    {
        super(cmds, model, config, craftedCommands);
    }

    @Override
    public List<String> execute()
    {
        String chroots[] = {"", Consts.ARCH_CHROOT};

        for(String chroot : chroots)
        {
            activateLocale(model.getSelectedSystemLocale(), chroot);
        }

        setLocale(model.getSelectedSystemLocale());

        setTimeZone(model.getSelectedTimeZone());
        setKeyboardLayout(model.getSelectedKeyboardLayout());

        return craftedCommands;
    }

    public void setTimeZone(String timeZone)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.TIMEZONE, timeZone);
        buildCommand(Consts.SET_TIMEZONE, replaceMap);
    }

    public void setKeyboardLayout(String layout)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.KEYBOARD_LAYOUT, layout);
        buildCommand(Consts.SET_X11_KEYBOARD_LAYOUT, replaceMap);
    }

    public void activateLocale(String locale, String chroot)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.LOCALE, locale);

        if(chroot.equals(""))
        {
            buildCommand(Consts.ACTIVATE_LOCALE, replaceMap);
            buildCommand(Consts.GENERATE_LOCALES, replaceMap);
        }
        else
        {
            // this is a hack - for some reasons in YAML is it not allowed to have
            // GENERATE_LOCALES: ***CHROOT*** locale-gen
            // don't know why
            buildCommand(Consts.ACTIVATE_LOCALE+"i", replaceMap);
            buildCommand(Consts.GENERATE_LOCALES+"i", replaceMap);
        }
    }

    public void setLocale(String locale)
    {
        val localeSplitted = locale.split(" ");
        if(localeSplitted.length > 0)
        {
            val replaceMap = new HashMap<String, String>();
            replaceMap.put(Consts.LOCALE, localeSplitted[0]);
            buildCommand(Consts.SET_LOCALE, replaceMap);
        }
    }
}
