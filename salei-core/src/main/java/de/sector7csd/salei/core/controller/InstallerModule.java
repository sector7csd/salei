package de.sector7csd.salei.core.controller;

import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public abstract class InstallerModule
{
    public InstallerModule(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        this(cmds, model, config, new ArrayList<>());
    }

    public InstallerModule(Map<String, Command> cmds, InstallationModel model, Config config, List<String> craftedCommands)
    {
        this.cmds               = cmds;
        this.model              = model;
        this.config             = config;
        this.craftedCommands    = craftedCommands;
    }

    protected final Map<String, Command>    cmds;
    protected final InstallationModel       model;
    protected final Config                  config;

    public abstract List<String>    execute();
    
    protected List<String>          craftedCommands;

    protected void buildCommand(String command)
    {
        buildCommand(command, null);
    }

    protected void buildCommand(String command, Map<String, String> replaceMap)
    {
        try
        {
            Command source  = cmds.get(command);
            Command dest    = new Command();

            dest.getParameters().addAll(source.getParameters());

            val destParas = dest.getParameters();

            if(replaceMap != null)
            {
                for(int idx = 0; idx < destParas.size(); idx++)
                {
                    for(val replaceKey : replaceMap.keySet())
                    {
                        var srcParam = destParas.get(idx);

                        int pos;
                        while( (pos = srcParam.indexOf(replaceKey)) != -1)
                        {
                            val part1 = srcParam.substring(0, pos);
                            val part2 = srcParam.substring(pos + replaceKey.length());

                            val replacement = replaceMap.get(replaceKey);
                            val result = String.format("%s%s%s", part1, replacement, part2);

                            destParas.set(idx, result);
                            srcParam = result;
                        }
                    }
                }
            }

            val sb = new StringBuilder();
            for(val para : destParas)
            {
                if(para != null)
                {
                    sb.append(para);
                    sb.append(" ");
                }
            }

            val craftedCommand = sb.toString();
            craftedCommands.add(craftedCommand);

            synchronized (model)
            {
                model.getOutput().add(craftedCommand);
            }

            log.info("crafted command: [{}]", craftedCommand);

        }
        catch(Exception e)
        {
            log.error("failed command [{}] with [{}]", command, replaceMap, e);
        }
    }

    protected String StringListToSimpleString(@NonNull List<String> stringList)
    {
        val sb = new StringBuilder();
        for(val string : stringList)
        {
            sb.append(string);
            sb.append(" ");
        }

        return sb.toString().trim();
    }
}
