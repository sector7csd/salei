package de.sector7csd.salei.core.processutils;

import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Slf4j
public class ProcessExecutor
{
    static public void executeProcess(String commandline, ProcessLineReaderInterface lineReaderInterface)
    {
        val parameters = commandline.split(" ");
        val pb = new ProcessBuilder(parameters);

        try
        {
            val p = pb.start();
            val stdOutReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            val stdErrReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while(p.isAlive())
            {
                Thread.sleep(250);
                processLineReader(stdOutReader, true, lineReaderInterface);
                processLineReader(stdErrReader, false, lineReaderInterface);
            }
        }
        catch(Exception e)
        {
            log.error("Exception", e);
        }

    }

    static protected void processLineReader(BufferedReader reader, boolean isStdOut, ProcessLineReaderInterface lineReaderInterface)
    {
        String line;

        try
        {
            while( (line = reader.readLine()) != null)
            {
                lineReaderInterface.onNewLine(line, isStdOut);
            }
        }
        catch(Exception e)
        {
            log.error("Exception", e);
        }
    }
}
