package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;

import java.util.List;
import java.util.Map;

public class InstallBaseSystem extends InstallerModule
{
    public InstallBaseSystem(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    @Override
    public List<String> execute()
    {
        buildCommand(Consts.UPDATE_KEYRING);
        buildCommand(Consts.INSTALL_BASE_SYSTEM);
        buildCommand(Consts.COPY_FILES);
        buildCommand(Consts.GENERATE_FSTAB);

        return craftedCommands;
    }
}
