package de.sector7csd.salei.core.model;

import lombok.val;

import java.util.ArrayList;

public class BaseModel
{
    protected ArrayList<ModelChangeListener> changeListeners = new ArrayList<>();

    public void addChangeListener(ModelChangeListener changeListener)
    {
        changeListeners.add(changeListener);
    }

    protected void fireModelChange(String reason)
    {
        for(val listener : changeListeners)
        {
            listener.onModelChanged(reason);
        }
    }
}
