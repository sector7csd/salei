package de.sector7csd.salei.core.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Packages
{
    @JsonProperty
    protected List<Group> kernels;

    @JsonProperty
    protected List<Group> groups;

    @JsonProperty
    protected List<Profile> profiles;
}
