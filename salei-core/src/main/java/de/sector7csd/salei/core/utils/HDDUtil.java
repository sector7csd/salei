package de.sector7csd.salei.core.utils;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
public class HDDUtil
{
    static public final String FILESYSTEM   = "FileSystem";
    static public final String MOUNTPOINT   = "MountPoint";
    static public final String PERMISSIONS  = "Permissions";
    static public final String USER         = "User";
    static public final String GROUP        = "Group";
    static public final String TOTAL        = "Total";
    static public final String FREE         = "Free";
    static public final String USED         = "Used";
    static public final String FREE_PERCENT = "Free Percent";

    static public final String PROC_MOUNTS  = "/proc/mounts";

    static public List<String> getHDDPartitions()
    {
        try
        {
            @Cleanup val bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(PROC_MOUNTS), StandardCharsets.UTF_8));
            String response;
            val stringBuilder = new StringBuilder();
            while ((response = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(response.replaceAll(" +", "\t") + "\n");
            }
            return Arrays.asList(stringBuilder.toString().split("\n"));
        } catch (IOException e)
        {
            log.error("Failed", e);
        }
        return null;
    }

    static public List<Map<String, String>> getMapMounts()
    {
        List<Map<String, String>> resultList = new ArrayList<>();

        val hddPartitions = getHDDPartitions();
        if(hddPartitions != null)
        {
            for (String mountPoint : hddPartitions)
            {
                val result = new HashMap<String, String>();
                val mount = mountPoint.split("\t");
                result.put(FILESYSTEM,      mount[2]);
                result.put(MOUNTPOINT,      mount[1]);
                result.put(PERMISSIONS,     mount[3]);
                result.put(USER,            mount[4]);
                result.put(GROUP,           mount[5]);
                result.put(TOTAL,           String.valueOf(new File(mount[1]).getTotalSpace()));
                result.put(FREE,            String.valueOf(new File(mount[1]).getFreeSpace()));
                result.put(USED,            String.valueOf(new File(mount[1]).getTotalSpace() - new File(mount[1]).getFreeSpace()));
                result.put(FREE_PERCENT,    String.valueOf(getFreeSpacePercent(new File(mount[1]).getTotalSpace(), new File(mount[1]).getFreeSpace())));
                resultList.add(result);
            }
        }

        return resultList;
    }

    static private Integer getFreeSpacePercent(long total, long free)
    {
        Double result = (Double.longBitsToDouble(free) / Double.longBitsToDouble(total)) * 100;
        return result.intValue();
    }
}
