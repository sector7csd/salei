package de.sector7csd.salei.core.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper=true)
public class Profile extends BaseItem
{
    @JsonProperty
    protected List<String>  groups;

    @JsonProperty
    protected List<String>  requires;

    public String toString()
    {
        return name;
    }
}
