package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.val;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstallUsers extends InstallerModule
{
    public InstallUsers(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    public InstallUsers(Map<String, Command> cmds, InstallationModel model, Config config, List<String> craftedCommands)
    {
        super(cmds, model, config, craftedCommands);
    }

    public List<String> execute()
    {
        setPassword("root", model.getRootPassword());

        for(val user : model.getUsers())
        {
            createGroup(user.getName());
            createUser(user.getShell(), user.getName(), user.getName(), user.getFullName());
            setPassword(user.getName(), user.getPassword());

            if(user.isSudoRight())
            {
                addUserToGroup(user.getName(), config.getSudoGroup());
            }
        }

        buildCommand(Consts.SETUP_SUDO);

        return craftedCommands;
    }

    public void addUserToGroup(String username, String group)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.USERNAME, username);
        replaceMap.put(Consts.GROUP, group);
        buildCommand(Consts.ADD_USER_TO_GROUP, replaceMap);
    }

    public void setPassword(String username, String password)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.USERNAME, username);
        replaceMap.put(Consts.PASSWORD, password);
        buildCommand(Consts.SET_PASSWORD, replaceMap);
    }

    public void createUser(String shell, String username, String group, String fullUserName)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.SHELL, shell);
        replaceMap.put(Consts.USERNAME, username);
        replaceMap.put(Consts.GROUP, group);
        replaceMap.put(Consts.FULLUSERNAME, fullUserName);
        buildCommand(Consts.CREATE_USER, replaceMap);
    }

    public void deleteUser(String username)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.USERNAME, username);
        buildCommand(Consts.DELETE_USER, replaceMap);
    }

    public void createGroup(String group)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.GROUP, group);
        buildCommand(Consts.CREATE_GROUP, replaceMap);
    }

    public void deleteGroup(String group)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.GROUP, group);
        buildCommand(Consts.DELETE_GROUP, replaceMap);
    }

    public void deleteHomeDir(String username)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.USERNAME, username);
        buildCommand(Consts.DELETE_HOMEDIR, replaceMap);
    }
}
