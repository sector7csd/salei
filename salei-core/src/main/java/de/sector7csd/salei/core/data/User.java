package de.sector7csd.salei.core.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User
{
    @JsonProperty
    protected String    name;

    @JsonProperty
    protected String    password;

    @JsonProperty
    protected String    passwordRepeat;

    @JsonProperty
    protected String    fullName;

    @Builder.Default
    @JsonProperty
    protected String    shell           = "/bin/bash";

    @JsonProperty
    protected boolean   sudoRight;
}
