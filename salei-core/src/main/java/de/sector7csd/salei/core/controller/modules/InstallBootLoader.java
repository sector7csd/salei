package de.sector7csd.salei.core.controller.modules;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.val;

public class InstallBootLoader extends InstallerModule
{
    public InstallBootLoader(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    @Override
    public List<String> execute()
    {
        if(model.isUefiMode())
        {
            buildCommand(Consts.GRUB_INSTALL_UEFI);
        }
        else
        {
            val replaceMap = new HashMap<String, String>();
            replaceMap.put(Consts.BOOTLOADERLOCATION, model.getBootloaderLocation());
            buildCommand(Consts.GRUB_INSTALL_BIOS, replaceMap);
        }

        buildCommand(Consts.GRUB_OS_PROBER);
        buildCommand(Consts.GRUB_MK_CONFIG);

        return craftedCommands;
    }
}
