package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.List;
import java.util.Map;

@Slf4j
public class InstallYay extends InstallerModule
{
    public InstallYay(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    @Override
    public List<String> execute()
    {
        if(model.isInstallYay())
        {
            // use the InstallUsers module to temporarily create user yay
            val users   = new InstallUsers(cmds, model, config, craftedCommands);

            users.createGroup(Consts.YAY);
            users.createUser(Consts.BIN_BASH, Consts.YAY, Consts.YAY, Consts.YAY);

            buildCommand(Consts.SETUP_SUDO_YAY);
            buildCommand(Consts.INSTALL_YAY);
            buildCommand(Consts.REMOVE_SUDO_YAY);

            users.deleteUser(Consts.YAY);
            users.deleteGroup(Consts.YAY);
            users.deleteHomeDir(Consts.YAY);
        }

        return craftedCommands;
    }
}
