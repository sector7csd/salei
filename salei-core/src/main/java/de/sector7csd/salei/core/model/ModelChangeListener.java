package de.sector7csd.salei.core.model;

public interface ModelChangeListener
{
    void onModelChanged(String reason);
}
