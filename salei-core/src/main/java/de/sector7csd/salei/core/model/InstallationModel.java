package de.sector7csd.salei.core.model;

import de.sector7csd.salei.core.data.Packages;
import de.sector7csd.salei.core.data.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper=true)
public class InstallationModel extends BaseModel
{
    public static final String          OUTPUT_CHANGED          = "OUTPUT_CHANGED";

    public enum InstallState            {READY_TO_INSTALL, INSTALLING, DONE};

    protected InstallState              installState            = InstallState.READY_TO_INSTALL;

    protected Packages                  packages;

    protected String                    selectedKernel;
    protected String                    selectedProfile;

    protected boolean                   uefiMode;
    protected List<Map<String, String>> mountpoints;

    protected boolean                   useLocalMirror          = false;
    protected String                    localMirror;

    protected String                    hostname                = "archlinux";

    protected boolean                   installYay              = true;

    protected String                    bootloaderLocation      = "/dev/sda";

    protected List<String>              timeZones               = new ArrayList<>();
    protected String                    selectedTimeZone;

    protected List<String>              keyboardLayouts         = new ArrayList<>();
    protected String                    selectedKeyboardLayout;

    protected List<String>              systemLocales           = new ArrayList<>();
    protected String                    selectedSystemLocale;

    protected String                    rootPassword;

    protected String                    rootPasswordRepeat;

    protected List<User>                users                   = new ArrayList<>();

    protected List<String>              output                  = new ArrayList<>();

    public void addOutputLine(String line)
    {
        output.add(line + System.lineSeparator());
        fireModelChange(OUTPUT_CHANGED);
    }
}
