package de.sector7csd.salei.core.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper=true)
public class Group extends BaseItem
{
    @JsonProperty
    protected List<String>  packages;

    @JsonProperty
    protected List<String>  exec;

    public String toString()
    {
        return name;
    }
}
