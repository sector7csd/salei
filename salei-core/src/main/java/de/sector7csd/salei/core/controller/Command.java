package de.sector7csd.salei.core.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Command
{
    @Builder.Default
    protected List<String>  parameters  = new ArrayList<>();
}
