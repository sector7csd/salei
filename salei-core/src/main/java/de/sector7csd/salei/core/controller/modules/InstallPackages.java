package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.data.Group;
import de.sector7csd.salei.core.data.Profile;
import de.sector7csd.salei.core.model.InstallationModel;
import de.sector7csd.salei.core.utils.InstallerUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.*;

import static de.sector7csd.salei.core.utils.InstallerUtils.indexOfList;

@Slf4j
public class InstallPackages extends InstallerModule
{
    protected List<String> pkgGroupsToInstall  = new ArrayList<>();

    public InstallPackages(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    public List<String> execute()
    {
        log.info("Collecting package groups");

        val profiles    = model.getPackages().getProfiles();
        val kernels     = model.getPackages().getKernels();

        val selectedProfile = profiles.get(InstallerUtils.getIndexOfStringInBaseItemList(model.getSelectedProfile(), profiles));

        // get selected kernel and add it's pkg group the installation list
        val kernel  = kernels.get(InstallerUtils.getIndexOfStringInBaseItemList(model.getSelectedKernel(), kernels));
        pkgGroupsToInstall.add(kernel.getName());

        // Check if our profile requires other profiles
        val requiredProfiles    = selectedProfile.getRequires();
        if(requiredProfiles     != null)
        {
            for (val requiredProfile : requiredProfiles)
            {
                int reqIdx = indexOfList(profiles, requiredProfile);
                val reqProfile = profiles.get(reqIdx);

                pkgGroupsToInstall.addAll(reqProfile.getGroups());

                log.info("Selected profile requires profile [{}] using now pkg groups [{}]", requiredProfile, pkgGroupsToInstall.size());
            }
        }

        pkgGroupsToInstall.addAll(selectedProfile.getGroups());

        // when we are in uefi mode, we need to install special efi packages
        if(model.isUefiMode())
        {
            pkgGroupsToInstall.add("efi");
        }

        log.info("Using now [{}] pkg groups [{}]", pkgGroupsToInstall.size(), pkgGroupsToInstall);

        installPackages(pkgGroupsToInstall);

        return craftedCommands;
    }

    protected void installPackages(List<String> pkgGroupsToInstall)
    {
        val groups              = model.getPackages().getGroups();
        val kernels             = model.getPackages().getKernels();

        for(val pkgGroupToInstall : pkgGroupsToInstall)
        {
            List<Group> usedGroup   = groups;
            int grpIdx              = indexOfList(groups, pkgGroupToInstall);
            if(grpIdx == -1)
            {
                // if lookup failed, try again with the kernels
                grpIdx              = indexOfList(kernels, pkgGroupToInstall);
                usedGroup           = kernels;
            }

            if(grpIdx != -1)
            {
                val group   = usedGroup.get(grpIdx);
                if(group.getPackages() != null)
                {
                    log.info("Have to install [{}]", group.getPackages());

                    val replaceMap = new HashMap<String, String>();
                    replaceMap.put(Consts.PKGLIST, StringListToSimpleString(group.getPackages()));

                    buildCommand(Consts.INSTALL_PKG, replaceMap);
                }

                if(group.getExec() != null)
                {
                    for(val exec : group.getExec())
                    {
                        val params = Arrays.asList(exec.split(" "));
                        log.info("Have to execute [{}]", exec);

                        val replaceMap = new HashMap<String, String>();
                        replaceMap.put(Consts.CMD, StringListToSimpleString(params));

                        buildCommand(Consts.EXEC_CHROOT_CMD, replaceMap);
                    }
                }
            }
            else
            {
                log.error("Unable to lookup [{}] pkg group", pkgGroupToInstall);
            }
        }
    }
}
