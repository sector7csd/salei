package de.sector7csd.salei.core.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class Config
{
    @JsonProperty
    Map<String, String> commands;

    @JsonProperty
    boolean debugMode;

    @JsonProperty
    String sudoGroup;

    @JsonProperty
    String defaultTimeZone;

    @JsonProperty
    String defaultKeyboardLayout;

    @JsonProperty
    String defaultLocale;

    @JsonProperty
    String defaultKernel;

    @JsonProperty
    String defaultProfile;

    @JsonProperty
    String localMirror;
}
