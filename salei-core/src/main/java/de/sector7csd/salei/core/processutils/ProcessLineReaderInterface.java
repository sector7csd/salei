package de.sector7csd.salei.core.processutils;

public interface ProcessLineReaderInterface
{
    void onNewLine(String line, boolean isStdOut);
}
