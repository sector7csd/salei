package de.sector7csd.salei.core.controller;

import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import de.sector7csd.salei.core.processutils.ProcessExecutor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class InstallerBase
{
    protected Map<String, Command>   cmds                = new HashMap<>();
    protected InstallationModel      model;
    protected Config                 config;

    public InstallerBase(InstallationModel model, Config config)
    {
        this.model = model;
        this.config = config;

        for(val key : config.getCommands().keySet())
        {
            val entry   = config.getCommands().get(key);
            val cmd  = Command.builder().parameters(Arrays.asList(entry.split(" "))).build();

            cmds.put(key, cmd);
        }
    }

    protected void processReader(BufferedReader reader, boolean isOutOrErr)
    {
        String line;

        try
        {
            while( (line = reader.readLine()) != null)
            {
            }
        }
        catch(Exception e)
        {
            log.error("Exception", e);
        }
    }

    protected void writeAndExecuteScript(List<String> craftedCommands)
    {
        try
        {
            val tempFile = File.createTempFile("install", ".sh");

            val writer = new BufferedWriter(new FileWriter(tempFile));

            writer.write(Consts.SHE_BANG_BASH);
            writer.newLine();

            for (val line : craftedCommands)
            {
                boolean showCommand = true;
                if (line.contains("echo"))
                {
                    showCommand = false;
                }

                val outline = line.trim();

                if (showCommand)
                {
                    writer.write(String.format("echo 'Executing [%s]'", outline));
                    writer.newLine();
                }

                writer.write(outline);
                writer.newLine();
            }

            writer.flush();
            writer.close();

            // execute
            if (!config.isDebugMode())
            {
                ProcessExecutor.executeProcess(String.format("%s %s", "sh", tempFile.getAbsolutePath()), (line, isStdOut) ->
                {
                    if (isStdOut)
                    {
                        log.info("StdOut: [{}]", line);
                    }
                    else
                    {
                        log.error("StdErr: [{}]", line);
                    }

                    synchronized (model)
                    {
                        model.addOutputLine(line);
                    }
                });
            }

            if(config.isDebugMode())
            {
                tempFile.delete();
            }
        }
        catch(Exception e)
        {
            log.error("Exception", e);
        }
    }

    public void setInstallationState(InstallationModel.InstallState installState)
    {
        synchronized (model)
        {
            model.setInstallState(installState);
        }
    }
}
