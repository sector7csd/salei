package de.sector7csd.salei.core.controller;

import de.sector7csd.salei.core.controller.modules.*;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.ArrayList;

@Slf4j
public class InstallationTask extends InstallerBase implements Runnable
{
    public InstallationTask(InstallationModel model, Config config)
    {
        super(model, config);
    }

    @Override
    public void run()
    {
        setInstallationState(InstallationModel.InstallState.INSTALLING);

        log.info("Start");
        model.getOutput().add("-- Building install script");

        val craftedCommands = new ArrayList<String>();

        InstallerModule[] modules = {
                                        new InstallMirror       (cmds, model, config),
                                        new InstallBaseSystem   (cmds, model, config),
                                        new InstallLocale       (cmds, model, config),
                                        new InstallPackages     (cmds, model, config),
                                        new InstallYay          (cmds, model, config),
                                        new InstallBootLoader   (cmds, model, config),
                                        new InstallUsers        (cmds, model, config),
                                        new InstallHostname     (cmds, model, config)
                                    };

        for(val module : modules)
        {
            craftedCommands.addAll(module.execute());
        }

        // write to script
        writeAndExecuteScript(craftedCommands);

        log.info("Done");

        setInstallationState(InstallationModel.InstallState.DONE);
    }
}
