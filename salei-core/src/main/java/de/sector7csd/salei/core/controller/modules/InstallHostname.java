package de.sector7csd.salei.core.controller.modules;

import de.sector7csd.salei.core.controller.Command;
import de.sector7csd.salei.core.controller.Consts;
import de.sector7csd.salei.core.controller.InstallerModule;
import de.sector7csd.salei.core.data.Config;
import de.sector7csd.salei.core.model.InstallationModel;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class InstallHostname extends InstallerModule
{
    public InstallHostname(Map<String, Command> cmds, InstallationModel model, Config config)
    {
        super(cmds, model, config);
    }

    @Override
    public List<String> execute()
    {
        setHostname(model.getHostname());

        return craftedCommands;
    }

    protected void setHostname(String hostname)
    {
        val replaceMap = new HashMap<String, String>();
        replaceMap.put(Consts.HOSTNAME, hostname);
        buildCommand(Consts.SET_HOSTNAME, replaceMap);
    }
}
