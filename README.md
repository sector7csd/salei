# Salei

stands for *Sector 7's Arch Linux Easy Installer*.

![Screenshot Salei-Web](https://bitbucket.org/sector7csd/salei/raw/43c15924b18eb91354c08fb5755dd2ea53bfca9e/screenshots/salei-web.png)

It is an GUI / TUI installer written Java and it uses a predefined package list in YAML. 
For the following popular desktop environments are default configurations created:

- Cinnamon
- XFCE
- LXDE
- KDE

Also, included are the following desktop environments - however they are not yet preconfigured:

- Budgie
- Deepin
- LXQT
- Mate

## Requirements

In order to be able to build you need the following:

- a JDK
- [Yarn](https://yarnpkg.com/) 
  can be installed using [npm](https://www.archlinux.org/packages/community/any/npm/)

## Building

```bash
yarn install
yarn build
./gradlew distTar
```

The tar's can be found, as usual for a [gradle](https://gradle.org/) project under each subproject in the 
**build/distributions** folder.

The resulting programs are expecting to be executed from their folder with **./bin/programname**, example:

```bash
cd salei-web-1.0-SNAPSHOT
./bin/salei-web
```

## Archlive

This project was created with my [ArchLive project](https://bitbucket.org/sector7csd/archlive/src/master/) in mind.

So you can use that as an environment to execute Salei-Web in.

## Official ArchLinux-CD and Salei-Cli installation  

If you wish you can use the official ArchLinux-CD with Salei-Cli to install your system.

When you booting from the CD, your / is a ram disk with 256 MB in size. If you install java runtime and Salei-Cli around
81% of that space will be used - if you're using jre8.  

All what you have to do is the following:

```bash
loadkeys de     # or other keyboard layout if needed
pacman -Sy jre8-openjdk-headless glibc wget
export JAVA_HOME=/usr/lib/jvm/default
```

- partition the disk with the tool you like, e.g. cfdisk /dev/sda

- format the partitions (for test purpose, I have a 50 GB hdd, 4 GB for swap, rest for /)

```bash
mkswap /dev/sda1
mkfs.ext4 /dev/sda2
```

- mount the created partitions

```bash 
swapon /dev/sda1
mount /dev/sda2 /mnt
```

- download and extract Salei-Cli

```bash
wget https://remote.sector7csd.de/downloads/salei/salei-cli-1.0-SNAPSHOT.tar
tar -xvf salei-cli-1.0-SNAPSHOT.tar
```

- start salei

```bash
cd salei-cli-1.0-SNAPSHOT
./bin/salei-cli
```

Do you selections and start the installation, afterwards you need to unmount the partitions. If you don't have a local Arch Linux mirror, then delete the content of that line, to use the normal mirrors.

```bash
swapoff /dev/sda1
umount /dev/sda2

# Reboot to the new system
reboot
```   